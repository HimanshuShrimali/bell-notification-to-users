# README #

### What is this repository for? ###

* It provides a method which you can directly use for bell notification to users just by passing the respective parameters.

### How do I get set up? ###

* Create a Custom Notification in Salesforce first. Follow the below steps to reach there:
>  
>> `Setup > Search "Custom Notification" in Quick Find > Click on "New" button`
>
> It should open a window like below. Enter the values as required and Save it:
    
![Custom Notification Image](https://bitbucket.org/HimanshuShrimali/bell-notification-to-users/raw/93e1c5b0001f658fe619e98c06da3b14ae33ab0d/image/customNotification.PNG)
  

* Place the below code in a method in apex class which will be used to send bell notification to users:

~~~~
	@future (callout = true)
    public static void notifyUser(String notificationTitle, String message, List<String> userId, String recordId) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm()
            + '/services/data/v46.0/actions/standard/customNotificationAction');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        List<CustomNotificationType> customNotification = new List<CustomNotificationType>([Select Id,CustomNotifTypeName from CustomNotificationType WHERE CustomNotifTypeName = 'custom_notification_name(not api_name)']);
        CustomNotificationActionInput input = new CustomNotificationActionInput();
        input.customNotifTypeId = customNotification.get(0).Id;
        input.recipientIds = userId;
        input.title = notificationTitle;
        input.body = message;
        input.targetId = recordId;
        CustomNotificationAction action = new CustomNotificationAction();
        action.inputs = new List<CustomNotificationActionInput>{input};
        req.setBody(JSON.serialize(action));
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
    }
    
    public class CustomNotificationAction
    {
        public List<CustomNotificationActionInput> inputs { get; set; }
    }

    public class CustomNotificationActionInput
    {
        public String customNotifTypeId { get; set; }
        public List<String> recipientIds { get; set; }
        public String title { get; set; }
        public String body { get; set; }
        public String targetId { get; set; }
    }
~~~~
* Replace `custom_notification_name(not api_name)` in the above code.
* Then let's say we want to send bell notification to owners of Account. So, we'll write something like below in a different method and call the above method:

~~~~
	List<Account> accountRecords = new List<Account>([SELECT Id, OwnerId FROM Account LIMIT 10]);
    List<String> ownerIds = new List<String>();
    String message = 'Check your account details and please confirm asap.';
	String title = 'Provide Feedback';
    ownerIds.add(String.valueOf(accountRecords[0].OwnerId));
    String recordId = accountRecords[0].Id;
    chatterUser(title, message, ownerIds, recordId);
~~~~
* This sending of bell notification to Account owners is just an example, you can always use it according to your requirements.